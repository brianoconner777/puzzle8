# Puzzle8

## About

Android implementation of a star algorithm to un-shuffle an image.
Built as part of [Google CS](https://www.cs-first.com/en/home) with Android course.</h3>

### Build Instructions :

Clone the project from GitHub:
```
$ git clone https://brianoconner777@bitbucket.org/brianoconner777/puzzle8.git
```


### Main Features :

The app consists of a single activity having three buttons to :
1. Take a photo
2. Shuffle button, to shuffle the image
3. Solve button, to solve the image using the Star algorithm.

More details on the star algorithm are [here](https://en.wikipedia.org/wiki/A*_search_algorithm)

### Contributing

Anyone interested in contributing can send their PR to the repo. Most welcome.